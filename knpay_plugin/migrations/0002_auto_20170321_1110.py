# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-21 11:10
from __future__ import unicode_literals

from django.db import migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('knpay_plugin', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymenttransaction',
            name='status',
            field=django_fsm.FSMField(choices=[('created', 'Created'), ('canceled', 'Canceled'), ('paid', 'Paid'), ('failed', 'Failed')], db_index=True, default='created', max_length=50, protected=True, verbose_name='Status'),
        ),
    ]
