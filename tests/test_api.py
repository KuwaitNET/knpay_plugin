from django.test import TestCase

from knpay_plugin import api, models


class TestKnpayAPI(TestCase):

    def setUp(self):
        pass

    def test_create_payment(self):
        instance, result = api.create_payment_transaction(123, gateway_code='testKNET')
        self.assertTrue(result, instance)
        self.assertEqual(models.PaymentTransaction.objects.filter(amount='123').count(), 1)

    def test_create_payment_w_extra(self):
        extra_values = {'key': 'val'}
        request = {}
        instance, result = api.create_payment_transaction(222, gateway_code='testKNET', extra=extra_values, request=request)
        self.assertTrue(result, instance)
        self.assertEqual(models.PaymentTransaction.objects.filter(amount='222').count(), 1)
        self.assertEqual(instance.extra['key'], 'val')
