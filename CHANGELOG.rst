0.2.8
-----
2022-06-12

- Enable the mock of communication with server to avoid network connection while testing.
- Remove couple of Python 2 code


0.2.6
-----
2020-06-26

- Payment Request support
- Start blacking files


0.2.5
-----
2020-06-26

- Django 2.2 support
- Update arabic translations

0.2.4
-----
2020-06-26

- Better logging
- Update arabic translations

0.2.3
-----
2020-03-23

- Remove python2 print statements

0.2.2
-----
2019-08-22

- Remove hard-coded values

0.2.1
-----
2019-08-20

- Ability to control customer notifications for payment requests
- Support for custom fields API

0.2.0
-----
2019-08-06

- Provision to create payment requests via API

0.1.8
-----
2019-03-15

- Python 3 compatible (upgraded requirements / tox
- Made the model respect the config choices
- Added `create_payment_transaction` which return a payment instance not just the payment url-

0.1.7
-----
2018-05-16

- publish arabic translation

0.1.6
-----
2018-05-16

- add arabic translation
- fix duplicate code from template tags
- add extra settings for hiding automated order number in confirmation page

0.1.5
-----
2018-04-12

- Fix CHANGELOG for latest version

0.1.4
-----
2018-04-12

- Move prepare_payload func to api module

0.1.3
-----
2017-10-10

- Plugin Failed response logger content type added

0.1.2
-----
2017-09-11

- languages form field updated to language
- updated todos
- updated docs
- added regex in conf

0.1.1
-----
2017-04-14

- Add API function
- Update config

0.1
---
2017-02-16

- Initial release.